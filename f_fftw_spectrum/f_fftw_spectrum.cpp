#include "blc_channel.h"
#include "blc_program.h"
#include <fftw3.h>
#include <unistd.h>
#include <math.h> //sqrtf
#include <pthread.h>

#define DEFAULT_OUTPUT_NAME "spectrum<pid>"

sem_t *sem_output, *sem_input;
int i=0;
struct timeval;
char const *display, *option_record;
int columns_nb=64, rows_nb=16;

static void init_output_channel(blc_channel *output_channel, char const *output_name, int output_length){

    //Create a channel or reopen a previous one.
    output_channel->create_or_open(output_name, BLC_CHANNEL_WRITE, 'FL32', 'NDEF', 1, output_length);
    output_channel->publish();
    
    blc_loop_try_add_waiting_semaphore(output_channel->sem_ack_data);
    blc_loop_try_add_posting_semaphore(output_channel->sem_new_data);
}

int main(int argc, char**argv){
    blc_channel input, output;
    char const *input_name, *output_name, *spectrum_option, *period_str;
    FILE *file=NULL;
    int ret;
    int output_length, period, pos=0;
    size_t i;
    fftwf_complex *intermediate;
    fftwf_plan fftw_plan;
    
    blc_program_add_option(&display, 'd', "display", NULL, "Display the result as text graph", NULL);
    blc_program_add_option(&output_name, 'o', "output", "blc_channel|-", "channel containing the fast fourier transformation, or '-' for terminal output", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&period_str, 'p', "period", "integer", "Period in ms (0 as fast as possible).", "0");
    blc_program_add_option(&spectrum_option, 's', "spectrum", NULL, "Get the square value of the fftw signal", NULL);
    blc_program_add_parameter(&input_name, "input blc_channel", 1, "Channel containing the sound data", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    SSCANF(1, period_str, "%d", &period);

    //Open input channel and check the type of values
    input.open(input_name, BLC_CHANNEL_READ);
    if (input.type!='FL32') EXIT_ON_CHANNEL_ERROR(&input,"Input must be of type 'FL32'");
    output_length=input.total_length/2+1;
    
    if (strcmp("-", output_name)==0){
        output.init('FL32', 'NDEF', 1, output_length);
        file=stdout;
    }
    else if (strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) {
    	asprintf((char**)&output_name, ":spectrum%d", getpid());
    	init_output_channel(&output, output_name, output_length);
    }
    else{
    	ret=sscanf(output_name, "%*[:/.^]%*[^/]%n", &pos);
    	if (pos==strlen(output_name)) 

init_output_channel(&output, output_name, output_length);   //C'est un blc_channel
    	else //C'est un format de fichier
    	{
    		if (strcmp(blc_get_filename_extension(output_name), "tsv")!=0) EXIT_ON_ERROR("Filename extension must be .tsv but your file is: '%s'.\nThis is to be sure you are not overwriting important file by mistake", output_name);
            output.init('FL32', 'NDEF', 1, output_length);
    		SYSTEM_ERROR_CHECK(file=fopen(output_name,"w"), NULL, "opening '%s'", output_name);
    	}
    }

    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);
    
    intermediate=fftwf_alloc_complex(output_length);
    fftw_plan = fftwf_plan_dft_r2c_1d(input.total_length, input.floats, intermediate , FFTW_ESTIMATE);

    BLC_COMMAND_LOOP(period*1000){
        fftwf_execute(fftw_plan); /* repeat as needed */
        FOR(i, output_length){
            output.floats[i]=sqrtf(intermediate[i][0]*intermediate[i][0]+intermediate[i][1]*intermediate[i][1])/input.total_length;
        }
        //if output is not a channel we export to standard output
        if (file) {
           // if (blc_loop_iteration && file==stdout && blc_output_terminal) blc_eprint_cursor_up(100);
            output.fprint_tsv(file);
        }
        if(display){
            blc_fprint_float_graph(stderr, output.floats, output.total_length, "spectrum", columns_nb, rows_nb, 1, 0, "Frequency", "Intensity");
            blc_eprint_cursor_up(rows_nb);
        }
    }

    fftwf_destroy_plan(fftw_plan);
    blc_eprint_cursor_down(rows_nb);    
    
    if (file) SYSTEM_ERROR_CHECK(fclose(file), -1, NULL);
    return EXIT_SUCCESS;
}

