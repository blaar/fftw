
fftw
====

Apply a fft on th signal using fftw3.

Install
=======

Install [blaar](blaar.org) then in the blaar directory:

OSX:

	brew install fftw
	git submodule add https://framagit.org/blaar/fftw
	./install.sh fftw


Ubuntu:

    sudo apt-get install fftw3-dev
    git submodule add https://framagit.org/blaar/fftw
    ./install.sh fftw

Usage:
======
	
	bin/f_fftw <blc_channel-in>
